#include <stdio.h>
#include <limits.h>

typedef struct {
    long long int v;
    long long int u;
    long long int val;
} EDGE;

int main()
{   
    long long int n, m, s; // n - liczba wierzchołków grafu
                 // m - liczba krawędzi grafu
                 // s - początkowy wierzchołek
    scanf("%lld %lld %lld", &n, &m, &s);
    
    long long int d[n];
    for(long long int i = 0; i < n; i++)
    {
        d[i] = (LLONG_MAX/2);
    }
    d[s] = 0;

    EDGE edges[m];

    for(long long int i = 0; i < m; i++)
    {
        scanf("%lld %lld %lld", &edges[i].v, &edges[i].u, &edges[i].val);
    }
    
    for(long long int i = 0; i < n-1; i++)
    {
        for(long long int j = 0; j < m; j++)
        {
            if(d[edges[j].v] + edges[j].val < d[edges[j].u])
            {
                if(d[edges[j].v] != (LLONG_MAX/2))
                {
                    d[edges[j].u] = d[edges[j].v] + edges[j].val;
                }
            }
        }
    }

    for(long long int j = 0; j < m; j++)
    {
        if(d[edges[j].v] + edges[j].val < d[edges[j].u])
        {
            if(d[edges[j].v] != LLONG_MAX/2)
            {
                printf("NIE");
                return 0;
            }
        }
    }
    
    
    for(long long int j = 0; j < n; j++)
    {
        if(d[j] != (LLONG_MAX/2) )
        {
            if(j != s)
            {
                printf("%lld %lld ", j, d[j]);
            }
        }   
    }
    return 0;
}
