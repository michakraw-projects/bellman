**Zadanie**

Dany jest skierowany graf ważony. Znajdź najkrótsze odległości do wszystkich wierzchołków z jednego wyróżnionego wierzchołka s.

**Wejście**

W pierwszym wierszu dane są liczby n, m , s, gdzie n ≤ 500, m ≤ 500*500, 0 ≤ s ≤ n, oznaczające odpowiednio liczbę wierzchołków, liczbę krawędzi i numer wierzchołka wyróżnionego. Następnie na wejściu podanych jest m wierszy zawierających po trzy liczby całkowite a, b, ,c, gdzie liczby 0 ≤ a,b ≤ n, -1 000 000 ≤ c ≤ 1 000 000, oznaczające krawędż skierowaną od wierzchołka a do wierzchołka b o koszcie c.

**Wyjście**

Jeśli w grafie istnieje cykl o ujemnym łącznym koszcie, to należy wypisać słowo NIE. W przeciwnym wypadku, należy wypisać odległości do wszystkich wierzchołków osiągalnych z wierzchołka s w kolejności od najmniejszego numeru (podając najpierw numer wierzchołka a za nim --- odległość), z tym, że nie należy wypisywać odległości do wierzchołka wyróżnionego.

**Przykład**

Dla danych wejściowych

3 2 1

0 1 10

1 2 20

poprawną odpowiedzią jest

2 20